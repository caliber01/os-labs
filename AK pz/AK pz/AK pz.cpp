// AK pz.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int _tmain(int argc, _TCHAR* argv[])
{
	// Y = 3,5x^2 + 7.2x >
	// x=4,5 = x(3.5x+7.2)
	// s=3
	// O
	// N =6

	float x = 4.5, a = 3.5, b = 7.2;
	long y[6], S = 3;
	_asm {
		finit
			lea	EBX, y
			mov ECX, 6
			fld x
			m1 : fld ST
			fmul a
			fadd b
			fmul ST, ST(1)
			fistp dword ptr[EBX]
			fiadd S
			add EBX, 4
			loop m1
	}

	// Y = (14*x^2 _ 5.6)^1/2
	// N = 6
	// x=6, S=3
	float y[6], a = 5.6;
	long b = 14, x = 6;
	_asm {
		finit
			lea EBX, y
			mov ECX, 6
			m1:	fild x
			fmul ST, ST(1)
			fimul b
			fadd a
			fsqrt
			fstp dword ptr[EBX]
			add x, 3
			loop m1

	}

	// 1.8
	// y = 1024
	// 3.2x^2 - 25
	// N=6, x=2, S=2

	float y[6], a = 3.2;
	long b = 1024, c = 25, x = 2;
	_asm {
		finit
			lea EBX, y
			mov ECX, b
			m1 : fild x
			fmul
			fimul a
			bsub c
			fidivR b
			fstp dwort ptr{ EBX }
			add EBX, y
			add x, 2
			loop m1

	}

	// 1.6 an = (4.5^n)/(n+5)
	// N=6, n=1, S=1
	// O - do celogo

	long y[6], b = 5, n = 1;
	float a = 4.5;

	_asm
	{
		finit
		lea EBX, y
			mov ECX. 6
			fld1
			fldz
			m1 : fld a
			fmulp ST(2), ST
			fild n
			fiadd b
			fdiv ST, ST(2)
			fadd
			fstp dwort ptr[EBX]
			add EBX, y
			inc n
			loop m1

	}

	// 2.1
	// y=20/(x^2+2.5&x) < 0.2

	float a = 2.5, b = 0.2;
	long c = 20, x = 0;

	_asm {
		finit
			fld1
			m1 : inc x
			fmul a
			fild x
			fmul ST, ST
			fadd ST, ST(1)
			fidivr c
			fcomp b
			fstsw ax
			sahf
			jnc m1
	}

	//b(i) = sin(2*i^2)
	//sum(b(i)>3)

	long i = 0, a = 3, b = 150;
	_asm {
		finit
			fldz
			m1 : inc i
			fild i
			fmul ST, ST
			fadd ST, ST
			fldpi
			fmul
			fidiv b
			fsin
			fadd
			ficom a
			fotsw / +x
			sahf
			jc m1
	}

	// 2.4 an = 3,3^n+5
	// sum(an > 15000)

	float c = 3.3;
	long n = 0, a = 15000, b = 5;
	_asm {
		finit
			fld1
			fldz
			m1 : inc n
			fld
			fmulp ST(2), ST
			fild b
			fadd ST(2)
			fadd
			ficom a
			fstsw AX
			sqhf
			jc m1
	}
	return 0;

	// 2.6 
	// Y= (2*3.5^x +10)^1/2 > 100


	float a = 3.5;
	long b = 10, c = 100;

	_asm {
		finit
			fld1
			m1 : inc x
			fmul a
			fld ST
			fadd ST, ST
			fiadd b
			fsqrt
			ficomp c
			fstsw AX
			sqhf
			jc m1
	}

	// 4.1) l = 5*ln(sinx)
	// n=7
	// x=10gradusov
	// S=15

	long a = 5, x = 10, b = 180;
	float Y[6];

	_asm {
		lea EBX, Y
			mov ECX, 6
			m1: fldln2
			fidpi
			fimul x
			fidiv b
			fsin
			fyl2x // y = log2x
			fimul a
			fstp dwordptr[EBX]
			add EBX, 4
			add x, 15
			loop m1
	}

	// 4.8)
	// y = y^(3^2+1)
	// N = 5
	// X = 0.2
	// S = 0.4

	long a = 4;
	float Y[5], x = 0.2, S = 0.4;
	
	_asm {
		lea EBX, Y
			mov ECX, 5
			fld X
			m1: fld ST
			fmul ST, ST
			fld1
			fadd

	}
	

}

