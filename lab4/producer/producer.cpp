// producer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>

void producer(DWORD number);
void produce();
TCHAR * createFile(DWORD globalIndex);

HANDLE producerSemaphore;
HANDLE consumerSemaphore;
HANDLE mapMutex;

int _tmain(int argc, _TCHAR* argv[])
{
	TCHAR * comLine = GetCommandLine();
	producer(0);
	return 0;
}

void producer(DWORD producerIndex) {
	producerSemaphore = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, _T("producerSemaphore"));
	consumerSemaphore = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, _T("consumerSemaphore"));
	mapMutex = OpenMutex(MUTEX_ALL_ACCESS, false, _T("mapMutex"));
	
	DWORD dwWaitResS;

	while (1) {

		dwWaitResS = WaitForSingleObject(producerSemaphore, INFINITE);
		switch (dwWaitResS)
		{
		case WAIT_OBJECT_0:
			produce();
			ReleaseSemaphore(consumerSemaphore, 1, NULL);
		}
	}
}

void produce() {

		HANDLE hMap = OpenFileMapping(FILE_MAP_ALL_ACCESS, false, _T("fileMapping"));
		PVOID pMap = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, 256);

		PDWORD mem = (PDWORD)pMap;
		
		DWORD dwWaitResM;
		dwWaitResM = WaitForSingleObject(mapMutex, INFINITE);
		_tprintf(_T("%d\n"), GetLastError());
		DWORD stackIndex = mem[0]++; 
		DWORD globalIndex = mem[1]++;
		mem[stackIndex + 2] = globalIndex;
		TCHAR * filePath = createFile(globalIndex);
		ReleaseMutex(mapMutex);
		//Sleep(2000);
		_tprintf(_T("producer created file: %s\n"), filePath);

}

TCHAR * createFile(DWORD globalIndex) {
	static TCHAR filePath[80];
	_stprintf_s(filePath, _T("file%d.txt"), globalIndex);
	HANDLE hFile = CreateFile(filePath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, 0, 0);
	DWORD dwCount;
	WriteFile(hFile, filePath, _tcslen(filePath) * sizeof(TCHAR), &dwCount, 0);
	CloseHandle(hFile);
	return filePath;
}
