// consumer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>

void consumer(DWORD consumerIndex);
void consume();
TCHAR * readFile(DWORD fileIndex);

HANDLE producerSemaphore;
HANDLE consumerSemaphore;
HANDLE mapMutex;

int _tmain(int argc, _TCHAR* argv[])
{
	consumer(0);
	return 0;
}

void consumer(DWORD consumerIndex) {

	producerSemaphore = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, _T("producerSemaphore"));
	consumerSemaphore = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, _T("consumerSemaphore"));
	mapMutex = OpenMutex(MUTEX_ALL_ACCESS, false, _T("mapMutex"));

	DWORD dwWaitResS;

	while (1) {
		dwWaitResS = WaitForSingleObject(consumerSemaphore, INFINITE);
		switch (dwWaitResS)
		{
		case WAIT_OBJECT_0:
			consume();
			ReleaseSemaphore(producerSemaphore, 1, NULL);
		}
	}
}

void consume() {
		HANDLE hMap = OpenFileMapping(FILE_MAP_ALL_ACCESS, false, _T("fileMapping"));
		PVOID pMap = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, 256);

		PDWORD mem = (PDWORD)pMap;

		DWORD dwWaitResM;
		dwWaitResM = WaitForSingleObject(mapMutex, INFINITE);
		DWORD stackIndex = --mem[0]; 
		DWORD fileIndex = mem[stackIndex + 2];
		UnmapViewOfFile(pMap);
		ReleaseMutex(mapMutex);

		TCHAR * fileContent = readFile(fileIndex);
		_tprintf(_T("file content: %s\n"), fileContent);
		//Sleep(2000);
}

TCHAR * readFile(DWORD fileIndex) {
	TCHAR filePath[80];
	_stprintf_s(filePath, _T("file%d.txt"), fileIndex);
	_tprintf(_T("consumer reading file %s\n"), filePath);
	HANDLE hFile = CreateFile(filePath, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_ALWAYS, 0, 0);
	DWORD dwCount;
	static TCHAR content[80];
	ReadFile(hFile, content, 80 * sizeof(TCHAR), &dwCount, 0);
	CloseHandle(hFile);
	DeleteFile(filePath);
	return content;
}


