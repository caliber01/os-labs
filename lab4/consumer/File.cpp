#include "stdafx.h"
#include "File.h"

File::File() {};

File::File(int index) {
	TCHAR buf[80];
	_stprintf_s(buf, _T("file%d.txt"), index);
	hFile = CreateFile(buf, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, 0, 0);
	DWORD dwCount;
	WriteFile(hFile, buf, _tcslen(buf) * sizeof(TCHAR), &dwCount, 0);
	_tcscpy_s(filePath, 80,  buf);
	Free();
	Sleep(1000);
}

TCHAR * File::Read(int index) {
	DWORD sizeHigh;
	DWORD sizeLow = GetFileSize(hFile, &sizeHigh);
	hFile = CreateFile(filePath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);

	TCHAR * fileContent = new TCHAR[80];
	DWORD dwCount;
	ReadFile(hFile, fileContent, 80, &dwCount, 0);
	fileContent[dwCount/sizeof(TCHAR)] = '\0';
	_tprintf(_T("%s\n"), fileContent);
	Free();
	Sleep(7000 + rand() % 2000);
	DeleteFile(filePath);
	return fileContent;
}

void File::Free() {
	CloseHandle(hFile);
}
