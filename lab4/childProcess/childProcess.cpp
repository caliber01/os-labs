// childProcess.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>

int _tmain(int argc, _TCHAR* argv[])
{
	HANDLE mutexHandle = CreateMutex(0, false, _T("myMutex"));
	OpenMutex(MUTEX_ALL_ACCESS, false , _T("myMutex"));
	WaitForSingleObject(mutexHandle, INFINITE);
	HANDLE fileHandle = CreateFile(_T("info.txt"), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 0, OPEN_ALWAYS, 0, 0);

	DWORD counter;
	DWORD dwCount;
	SetFilePointer(fileHandle, 0, 0, FILE_BEGIN);
	ReadFile(fileHandle, &counter, 4, &dwCount, 0);
	counter++;
	SetFilePointer(fileHandle, 0, 0, FILE_BEGIN);
	WriteFile(fileHandle, &counter, 4, &dwCount, 0);
	SetEndOfFile(fileHandle);
	CloseHandle(fileHandle);
	ReleaseMutex(mutexHandle);


	_tprintf(_T("Thread started\n"));
	for (int i = 0; i < 1000000000; i++) {

	}
	_tprintf(_T("Thread finished\n"));

	return 0;
}

