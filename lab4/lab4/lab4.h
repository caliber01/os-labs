#pragma once

#include <Windows.h>
#include <list>
#include "File.h"

using namespace std;

// #typedef unsigned int (__stdcall * ST_ADR)(void *)
// #define CTU(FuncName, par, idAddr) (HANDLE) _BeginThreadEx(0,0,(ST_ADR) FuncName, 0)

#define NewThread CreateThread

// struct for running threads and processes
struct {
	CRITICAL_SECTION cs;
	HANDLE mutexHandle;
	int threadCounter;
}RUN_THREADS ;

// struct for checking number of threads
struct {
	CRITICAL_SECTION cs;
	int threadsCreated;
	int threadsActuallyRun;
	bool approachedLimit;
	HANDLE fileHandle;
}CHECK_THREADS ;


// structure for producer and consumer thread data
typedef struct {
	int index;
} PAR, *PPAR;


// structure for producer and consumer configuration
struct {
	File files[10];
	int index;
	HANDLE indexMutex;
	HANDLE fileCounterMutex;
	HANDLE consumerSemaphore;
	HANDLE producerSemaphore;
	HANDLE hProducerThreads[10];
	HANDLE hConsumerThreads[10];
	PPAR consumerPars;
	PPAR producerPars;
	int fileCounter;
	int N;
} PRODCONS;

// structure for producer and consumer configuration in processes
struct {
	File files[10];
	int index;
	HANDLE mapMutex;
	HANDLE fileCounterMutex;
	HANDLE consumerSemaphore;
	HANDLE producerSemaphore;
	HANDLE hProducerProcesses[10];
	HANDLE hConsumerProcesses[10];
	int fileCounter;
	int N;
} PRODCONSPROC;
