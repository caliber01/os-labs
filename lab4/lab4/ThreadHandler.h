#pragma once

class ThreadHandler {
public: 
	ThreadHandler();
	void runThreads();
	void runProcesses();
	void checkThreads();
	void producerConsumer();
	void producerConsumerProcesses();
private:
};
