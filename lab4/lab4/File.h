#pragma once

#include <Windows.h>

class File {
public:
	File();
	File(int index) ;
	TCHAR * Read(int index);
	void Free();
private:
	TCHAR filePath[80];
	HANDLE hFile;
};

