#include "stdafx.h"
#include "ThreadHandler.h"
#include <Windows.h>
#include "lab4.h"

DWORD WINAPI threadFunc(PVOID param) {
	EnterCriticalSection(&RUN_THREADS.cs);
	RUN_THREADS.threadCounter++;
	LeaveCriticalSection(&RUN_THREADS.cs);
	_tprintf(_T("Thread started\n"));
	for (int i = 0; i < 10000000; i++) {

	}
	_tprintf(_T("Thread finished\n"));
	return 0;

}

DWORD WINAPI checkThreadsFunc(PVOID param) {
	EnterCriticalSection(&CHECK_THREADS.cs);
	CHECK_THREADS.threadsActuallyRun++;
	_tprintf(_T("%d\n"), CHECK_THREADS.threadsActuallyRun);
	if (abs(CHECK_THREADS.threadsActuallyRun - CHECK_THREADS.threadsCreated) != 0) {
		CHECK_THREADS.approachedLimit = true;
	}
	LeaveCriticalSection(&CHECK_THREADS.cs);
	Sleep(INFINITE);

	_tprintf(_T("Thread finished\n"));
	return 0;
}



DWORD WINAPI producer(PVOID param) {
	PAR par = *(PPAR)param;
	_tprintf(_T("Producer#%d started\n"), par.index);
	DWORD dwWaitResS;
	DWORD dwWaitResM;
	while (1) {
		dwWaitResS = WaitForSingleObject(PRODCONS.producerSemaphore, INFINITE);
		switch (dwWaitResS)
		{
		case WAIT_OBJECT_0:
			WaitForSingleObject(PRODCONS.fileCounterMutex, INFINITE);
			File f = *new File(PRODCONS.fileCounter++);
			int fileCounter = PRODCONS.fileCounter;
			ReleaseMutex(PRODCONS.fileCounterMutex);
			dwWaitResM = WaitForSingleObject(PRODCONS.indexMutex, INFINITE);
			PRODCONS.files[PRODCONS.index++] = f;
			_tprintf(_T("Producer#%d, index: %d real: %d\n"), par.index, PRODCONS.index, fileCounter);
			ReleaseMutex(PRODCONS.indexMutex);
			ReleaseSemaphore(PRODCONS.consumerSemaphore, 1, NULL);
		}
	}
}

DWORD WINAPI consumer(PVOID param) {
	PAR par = *(PPAR)param;
	_tprintf(_T("Consumer#%d started\n"), par.index);
	DWORD dwWaitResS;
	DWORD dwWaitResM;

	while (1) {
		dwWaitResS = WaitForSingleObject(PRODCONS.consumerSemaphore, INFINITE);
		switch (dwWaitResS)
		{
		case WAIT_OBJECT_0:
			dwWaitResM = WaitForSingleObject(PRODCONS.indexMutex, INFINITE);
			File f = PRODCONS.files[--PRODCONS.index];
			_tprintf(_T("Consumer#%d "), par.index);
			ReleaseMutex(PRODCONS.indexMutex);
			ReleaseSemaphore(PRODCONS.producerSemaphore, 1, NULL);
			f.Read(PRODCONS.index);
		}
	}
}

ThreadHandler::ThreadHandler() {

}

void ThreadHandler::runThreads() {
	LPVOID param;
	DWORD id;
	HANDLE handles[10];

	int threadCreated = 0;

	InitializeCriticalSection(&RUN_THREADS.cs);
	for (int i = 0; i < 10; i++) {
		handles[i] = CreateThread(0, 0, threadFunc, &param, 0, &id);
	}
	WaitForMultipleObjects(10, handles, true, INFINITE);
	DeleteCriticalSection(&RUN_THREADS.cs);
	_tprintf(_T("thread counter: %d"), RUN_THREADS.threadCounter);
	getchar();
}

void ThreadHandler::runProcesses() {
	HANDLE handles[10];
	TCHAR exePath[] = _T("C://Users//������//Documents//Visual Studio 2015//Projects//os-labs//lab4//Debug//childProcess.exe");
	HANDLE mutexHandle = CreateMutex(0, false, _T("myMutex"));

	HANDLE fileHandle = CreateFile(_T("info.txt"), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, 0, 0);
	DWORD initCounter = 0;
	DWORD dwCount;
	WriteFile(fileHandle, &initCounter, 4, &dwCount, 0);
	CloseHandle(fileHandle);

	for (size_t i = 0; i < 11; i++)
	{
		STARTUPINFO si;
		PROCESS_INFORMATION pi;

		ZeroMemory(&si, sizeof(si));
		si.cb = sizeof(si);
		ZeroMemory(&pi, sizeof(pi));
		bool b = CreateProcess(0, exePath, 0, 0, 0, 0, 0, 0, &si, &pi);
		handles[i] = pi.hThread;
	}
	WaitForMultipleObjects(10, handles, true, INFINITE);
}

void ThreadHandler::checkThreads() {
	HANDLE handles[10000];

	InitializeCriticalSection(&CHECK_THREADS.cs);
	for (int i = 0; i < 10000; i++) {
		if (!CHECK_THREADS.approachedLimit) {
			EnterCriticalSection(&CHECK_THREADS.cs);
			CHECK_THREADS.threadsCreated++;
			LeaveCriticalSection(&CHECK_THREADS.cs);
			handles[i] = CreateThread(0, 0, checkThreadsFunc, 0, 0, 0);
			WaitForSingleObject(handles[i], 20);
		}
		else break;
	}

	WaitForMultipleObjects(CHECK_THREADS.threadsActuallyRun, handles, true, INFINITE);
	DeleteCriticalSection(&CHECK_THREADS.cs);
	_tprintf(_T("%d"), CHECK_THREADS.threadsActuallyRun);
	getchar();
}

void ThreadHandler::producerConsumer() {
	int N = 10;
	PRODCONS.N = N;
	PRODCONS.index = 0;

	PRODCONS.producerSemaphore = CreateSemaphore(0, N, N, 0);
	PRODCONS.consumerSemaphore = CreateSemaphore(0, 0, N, 0);
	PRODCONS.indexMutex = CreateMutex(0, false, 0);
	PRODCONS.fileCounterMutex = CreateMutex(0, false, 0);
	PRODCONS.consumerPars = new PAR[N];
	PRODCONS.producerPars = new PAR[N];

	for (size_t i = 0; i < N; i++)
	{
		PRODCONS.consumerPars[i].index = i;
		PRODCONS.hConsumerThreads[i] = CreateThread(0, 0, consumer, (PVOID)&PRODCONS.consumerPars[i], 0, 0);
		PRODCONS.producerPars[i].index = i;
		PRODCONS.hProducerThreads[i] = CreateThread(0, 0, producer, (PVOID)&PRODCONS.producerPars[i], 0, 0);
	}

	WaitForMultipleObjects(N, PRODCONS.hConsumerThreads, true, INFINITE);
	WaitForMultipleObjects(N, PRODCONS.hProducerThreads, true, INFINITE);
}

void ThreadHandler::producerConsumerProcesses() {

	int N = 10;
	PRODCONSPROC.N = N;
	PRODCONSPROC.index = 0;

	PRODCONSPROC.producerSemaphore = CreateSemaphore(0, N, N, _T("producerSemaphore"));
	PRODCONSPROC.consumerSemaphore = CreateSemaphore(0, 0, N, _T("consumerSemaphore"));
	PRODCONSPROC.mapMutex = CreateMutex(0, false, _T("mapMutex"));

	HANDLE hMap = CreateFileMapping(INVALID_HANDLE_VALUE, 0, PAGE_READWRITE, 0, 256, _T("fileMapping"));
	PVOID pBuf = (LPTSTR)MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, 256);
	PDWORD mem = (PDWORD)pBuf;
	mem[1] = 1;
	mem[0] = 0;
	UnmapViewOfFile(pBuf);

	for (size_t i = 0; i < 10; i++)
	{
		STARTUPINFO siCons;
		PROCESS_INFORMATION piCons;
		STARTUPINFO siProd;
		PROCESS_INFORMATION piProd;

		ZeroMemory(&siCons, sizeof(siCons));
		siCons.cb = sizeof(siCons);
		ZeroMemory(&piCons, sizeof(piCons));

		ZeroMemory(&siProd, sizeof(siProd));
		siProd.cb = sizeof(siProd);
		ZeroMemory(&piProd, sizeof(piProd));

		TCHAR consumerName[MAX_PATH];
		TCHAR producerName[MAX_PATH];
		_stprintf_s(consumerName,_T("consumer.exe %d"), (DWORD)i);
		_stprintf_s(producerName,_T("producer.exe %d"), (DWORD)i);

		CreateProcess(0, consumerName, 0, 0, 0, 0, 0, 0, &siCons, &piCons);
		CreateProcess(0, producerName, 0, 0, 0, 0, 0, 0, &siProd, &piProd);

		PRODCONSPROC.hConsumerProcesses[i] = piCons.hProcess;
		PRODCONSPROC.hProducerProcesses[i] = piProd.hProcess;
	}

	WaitForMultipleObjects(10, PRODCONSPROC.hConsumerProcesses, true, INFINITE);
	WaitForMultipleObjects(10, PRODCONSPROC.hProducerProcesses, true, INFINITE);
}
