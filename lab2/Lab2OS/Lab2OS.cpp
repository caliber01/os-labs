// Lab2OS.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <stdio.h>

int countDrives();
UINT* getDrives();



int countDrives(){
	DWORD d = GetLogicalDrives();
	int count = 0;
	for (int i = 0; i < 26; i++){
		char c = (char)((int)'A' + i);
		int available = (d >> i) & 1;
		if (available == 1){
			count++;
		}
		//_tprintf(_T("%c %d\n"), c, available);
	}
	return count;
}

UINT* getDrives(){
	TCHAR buf[255];
	DWORD d = GetLogicalDriveStrings(sizeof(TCHAR)* 255, buf);
	
	TCHAR ** driveNames = new TCHAR *[10];
	UINT driveTypes[10];
	for (int i = 0; i < _tcslen(buf); i+=4){
		TCHAR driveName[4]; 
		for (DWORD j = 0; j < 4; j++)
		{
			driveName[j] = buf[i + j];
		}
		driveNames[i / 4] = driveName;
		driveNames[i / 4 + 1] = 0;
		driveTypes[i / 4] = GetDriveType(driveName);
		driveTypes[i / 4 + 1] = 0;
	}
	for (DWORD i = 0; i < driveTypes[i] != 0; i++)
	{
		TCHAR * type;
		switch (driveTypes[i]){
		case 0:
			type = _T("DRIVE_UNKNOWN");
			break;
		case 1:
			type = _T("DRIVE_NO_ROOT_DIR");
			break;
		case 2:
			type = _T("DRIVE_REMOVABLE");
			break;
		case 3:
			type = _T("DRIVE_FIXED");
			break;
		case 4:
			type = _T("DRIVE_REMOTE");
			break;
		case 5:
			type = _T("DRIVE_CDROM");
			break;
		case 6:
			type = _T("DRIVE_RAMDISK");
			break;
		}
		_tprintf(_T("%s %s\n"), driveNames[i], type);
	}
	return driveTypes;
}

