// OS Lab 1.cpp : Defines the entry poDWORD for the console application.

#include "stdafx.h"
#include "Mailbox.h"

int initMenu(){
	_tprintf(_T("%s\n"), _T("Create box - press 1"));
	_tprintf(_T("%s\n"), _T("Open box - press 2"));
	_tprintf(_T("%s\n"), _T("Close box - press 3"));
	_tprintf(_T("%s\n"), _T("Print info - press 4"));
	_tprintf(_T("%s\n"), _T("Read letter - press 5"));
	_tprintf(_T("%s\n"), _T("Read and delete letter - press 6"));
	_tprintf(_T("%s\n"), _T("Add letter - press 7"));
	_tprintf(_T("%s\n"), _T("Delete letter - press 8"));
	_tprintf(_T("%s\n"), _T("Delete all letters - press 9"));
	_tprintf(_T("%s\n"), _T("Exit - press 0"));
	TCHAR * response = new TCHAR[5];
	_tscanf_s(_T("%s"), response, 5);
	return _ttoi(response);
}

//DWORD _tmain(DWORD argc, _TCHAR* argv[])
//{
//	bool b = true;
//	Mailbox *m = nullptr;
//	while (b){
//
//		system("cls");
//		TCHAR * info = _T("");
//		if (m != nullptr){
//			info = m->getInfo();
//		}
//		_tprintf(_T("%s\n"), info);
//		int a = initMenu();
//
//		TCHAR * response;
//		TCHAR * fileName;
//		DWORD id;
//		bool deleteThen;
//		int maxLetters;
//		TCHAR * letter;
//
//		system("cls");
//		_tprintf(_T("%d"), a);
//		system("cls");
//		switch (a){
//		case 0:
//			//exit
//
//			break;
//		case 1:
//			// create box
//			_tprintf(_T("%s\n"), _T("Insert file name"));
//			fileName = new TCHAR[255];
//			_tscanf_s(_T("%s"), fileName, 255);
//			_tprintf(_T("%s\n"), _T("Insert max messages"));
//			response = new TCHAR[5];
//			_tscanf_s(_T("%s"), response, 5);
//			maxLetters = _ttoi(response);
//			system("cls");
//			m = new Mailbox(maxLetters, fileName);
//			_tprintf(_T("%s\n"), _T("SUCCESS"));
//			//_tscanf_s(_T("%s"), response);
//			break;
//		case 2:
//			//open box
//			_tprintf(_T("%s\n"), _T("Insert file name"));
//			response = new TCHAR[255];
//			_tscanf_s(_T("%s"), response, 255);
//			if (m != nullptr){
//				delete(m);
//			}
//			m = new Mailbox(response);
//			_tprintf(_T("%s\n"), _T("SUCCESS"));
//			break;
//		case 3:
//			//close box
//			break;
//		case 4:
//			//print info
//			info = m->getInfo();
//			_tprintf(_T("%s\n"), info);
//			getchar();
//			break;
//		case 5:
//			// read letter
//			_tprintf(_T("%s\n"), _T("Insert letter id"));
//			response = new TCHAR[255];
//			_tscanf_s(_T("%s"), response, 255);
//			id = _ttoi(response);
//			_tprintf(_T("%s\n"), _T("Delete letter then? Y/N"));
//			response = new TCHAR[1];
//			_tscanf_s(_T("%s"), response, sizeof(TCHAR));
//			deleteThen = response[0] == _T('Y');
//			letter = m->readLetter(id, deleteThen);
//			_tprintf(_T("%s\n"), letter);
//			_getch();
//			break;
//		case 7:
//			//add letter
//			_tprintf(_T("%s\n"), _T("Insert letter content:"));
//			response = new TCHAR[255];
//			_tscanf_s(_T("%s"), response, 255);
//
//			m->addLetter(response);
//			_tprintf(_T("%s\n"), _T("SUCCESS"));
//			break;
//		case 8:
//			//delete letter
//			_tprintf(_T("%s\n"), _T("Insert letter id:"));
//			response = new TCHAR[255];
//			_tscanf_s(_T("%s"), response, 255);
//			id = _ttoi(response);
//			m->deleteLetter(id);
//			break;
//		case 9:
//			// delete all letters
//			_tprintf(_T("%s\n"), _T("Are you sure? Y/N"));
//			response = new TCHAR[1];
//			_tscanf_s(_T("%s"), response, sizeof(TCHAR));
//			if (response[0] == _T('Y')){
//				m->deleteAllLetters();
//			}
//			break;
//		}
//	}
//	return 0;
//}

Letter::Letter(DWORD startIndex, DWORD length){
	this->length = length;
	this->startIndex = startIndex;
}

Mailbox::Mailbox(TCHAR * fileName) {
	TCHAR fullPath[80];
	_tcscpy(fullPath, path);
	_tcscat(fullPath, fileName);
//	_tcscat(fullPath, type);
	_tprintf(_T("%s\n"), fullPath);
	handle = CreateFile(fullPath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	bool a = handle == INVALID_HANDLE_VALUE;
	hMap = CreateFileMapping(handle, 0, PAGE_READWRITE, 0, 4096, 0);
	DWORD p = GetLastError();
	if (hMap){
		pMap = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, 4096);
	}
}

Mailbox::Mailbox(DWORD maxMessages, TCHAR * fileName) {
	TCHAR fullPath[80];
	_tcscpy(fullPath, path);
	_tcscat(fullPath, fileName);
	//_tcscat(fullPath, type);
	handle = CreateFile(fullPath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	hMap = CreateFileMapping(handle, 0, PAGE_READWRITE, 0, 4096, 0);
	if (hMap){
		pMap = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, 4096);
	}
	DWORD header[] = {0,0,maxMessages};
	writeHeader(header);
}

Mailbox::~Mailbox(){
	if (pMap) UnmapViewOfFile(pMap);
	if (hMap) CloseHandle(hMap);
	if (handle) CloseHandle(handle);
}

DWORD* Mailbox::readFileHeader(){
	DWORD header[3];
	PDWORD mem = (PDWORD)pMap;
	for (DWORD i = 0; i < 3; i++)
	{
		header[i] = mem[i];
	}

	DWORD * headerCpy = new DWORD[3];
	memcpy(headerCpy, header,12);
	DWORD i = headerCpy[2];
	return headerCpy;
}

void Mailbox::addLetter(TCHAR content[]){
	DWORD * fileHeader = readFileHeader();
	if (fileHeader[0] >= fileHeader[2]) {
		return;
	}
	DWORD totalLength = fileHeader[0] * 4 + fileHeader[1] + 12;
	DWORD messageLength = _tcslen(content)*sizeof(TCHAR);
	PBYTE mem = (PBYTE)pMap;
	mem += totalLength;
	*mem = messageLength;
	mem += 4;
	TCHAR * tMem = (TCHAR *)mem;
	_tcscpy(tMem, content);
	DWORD newHeader[] = { fileHeader[0] + 1, fileHeader[1] + messageLength, fileHeader[2] };
	writeHeader(newHeader);
}

void Mailbox::writeHeader(DWORD* newHeader){
	PDWORD mem = (PDWORD)pMap;
	for (DWORD i = 0; i < 3; i++)
	{
		mem[i] = newHeader[i];
	}
}

Letter* Mailbox::getLetter(DWORD id){
	DWORD * header = readFileHeader();
	DWORD max = header[0];
	if (id > max){
		_tprintf(_T("%s\n"),_T("Out of range"));
		return nullptr;
	}
	
	DWORD byteIndex = 12, i = 0;
	DWORD letterSize = *(DWORD*)readMemory(byteIndex, 4);;
	for (; i < id; i++){
		letterSize = *(DWORD*)readMemory(byteIndex,4);
		byteIndex += 4 + letterSize;
	}
	return new Letter(byteIndex+4, letterSize);
}

PBYTE Mailbox::readMemory(DWORD start, DWORD length){
	PBYTE bytes = (PBYTE)pMap;
	PBYTE result = new BYTE[length];
	for (DWORD i = start, j = 0; i < start+length; i++, j++)
	{
		result[j] = bytes[i];
	}
	return result;
}

TCHAR * Mailbox::readLetter(DWORD id, bool deleteThen){
	TCHAR * mem = (TCHAR*)pMap;
	DWORD * fileHeader = readFileHeader();
	if (id > fileHeader[0]){
		return nullptr;
	}
	Letter * l = getLetter(id);
	TCHAR * letter = new TCHAR[l->length/sizeof(TCHAR)];
	DWORD i,j;
	for (i = l->startIndex/sizeof(TCHAR), j = 0; i < l->startIndex/sizeof(TCHAR)+l->length/sizeof(TCHAR); i++, j++)
	{
		letter[j] = mem[i];
	}
	letter[j] = 0;
	if (deleteThen){
		deleteLetter(id);
	}
	return letter;
}

void Mailbox::deleteLetter(DWORD id) {
	BYTE * mem = (BYTE *)pMap;
	DWORD * header = readFileHeader();
	Letter * deleted = getLetter(id);
	DWORD startIndex = deleted->startIndex - 4;
	BYTE ending[255];
	size_t j = 0;
	// filling ending
	for (DWORD i = id+1; i < header[0]; i++)
	{
		Letter * l = getLetter(i);
		for (size_t k = l->startIndex - 4; k < l->length + l->startIndex; k++, j++)
		{
			ending[j] = mem[k];
		}
	}
	// deleting old
	for (size_t i = deleted->startIndex - 4; i < deleted->startIndex + deleted->length; i++)
	{
		mem[i] = 0;
	}
	// moving ending to the left
	size_t i = 0;
	for (; i < j; i++)
	{
		mem[i + startIndex] = ending[i];
	}
	// clearing until the end of file
	DWORD fileSize = header[1] + header[0] * 4 + 12;
	for (i += startIndex; i < fileSize; i++)
	{
		mem[i] = 0;
	}
	DWORD * newHeader = new DWORD[] {header[0]-1,header[1]-deleted->length,header[2]};
	writeHeader(newHeader);
}

DWORD Mailbox::countLetters(){
	return readFileHeader()[0];
}

DWORD Mailbox::generateChecksum(){

	SetFilePointer(handle, 0, NULL, FILE_BEGIN);
	return -1;
}

void Mailbox::deleteAllLetters(){
	DWORD * fileHeader = readFileHeader();
	DWORD newHeader[] = { 0, 0, fileHeader[2] };
	DWORD fileSize = fileHeader[0] * 4 + 12 + fileHeader[1];
	PBYTE mem = (PBYTE)pMap;
	for (size_t i = 12; i < fileSize; i++)
	{
		mem[i] = 0;
	}
	writeHeader(newHeader);
}

TCHAR * Mailbox::getInfo(){
	DWORD * header = readFileHeader();
	TCHAR info[80];
	_stprintf(info, _T("Messages: %d Bytes: %d MaxMessages: %d"), header[0], header[1], header[2]);
	return info;
}

DWORD getLength(DWORD * a){
	return sizeof(a) / sizeof(a[0]);
}
