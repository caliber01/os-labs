#include "stdafx.h"
#include "memory.h"
#include <algorithm>



int _tmain(int argc, TCHAR * argv[])
{
	/*Pages *p = new Pages(3);
	int order[6] = {1,2,3,2,3,5};
	p->UsePages(order, 6);
	p->PrintState();
	*/
	/*lru *l = new lru(4);
	int order[] = { 1, 2, 3, 4, 1, 2, 5, 1,2,3 };
	l->load(order, 10);*/

	Memory *m = new Memory();
	m->PrintRegions();
}

Region::Region(LPVOID address, DWORD size){
	Address = address;
	Size = size;
}
bool Region::operator==(Region other){
	return (this->Address == this->Address) && (this->Size == this->Size);
}

Memory::Memory(){

	SYSTEM_INFO info;
	GetSystemInfo(&info);
	info.dwPageSize;
	info.dwAllocationGranularity;

	handle = GetCurrentProcess();
	MEMORY_BASIC_INFORMATION buf;
	LPVOID currentAddress = info.lpMinimumApplicationAddress;

	while (currentAddress < info.lpMaximumApplicationAddress)
	{
		VirtualQueryEx(handle, currentAddress, &buf,sizeof(buf));

		Region *r = new Region(currentAddress, buf.RegionSize);

		if (buf.State == MEM_COMMIT){
			Full.push_back(*r);
		}
		else{
			Free.push_back(*r);
		}
		currentAddress = LPVOID((DWORD)currentAddress + r->Size);
		
	}
}
LPVOID Memory::AllocateSpace(DWORD size){
	Region *minimal = nullptr;
	for (std::list<Region>::iterator i = Free.begin(); i != Free.end(); ++i)
	{
		Region current = *i;
		if (current.Size >= size && current.Size < minimal->Size){
			minimal = &current;
		}
	}

	Full.push_back(*new Region(minimal->Address, size));
	//Free.remove(minimal);
	minimal->Address = (LPVOID)((DWORD)minimal->Address + size);
	minimal->Size -= size;
	return VirtualAllocEx(handle, minimal->Address, minimal->Size, MEM_COMMIT, 0);
}
void Memory::FreeSpace(LPVOID address){
	for (std::list<Region>::iterator i = Full.begin(); i != Full.end(); ++i)
	{
		Region current = *i;
		if (current.Address == address){
			Full.remove(current);
			Free.push_back(current);
			VirtualFreeEx(handle, current.Address, current.Size, MEM_RELEASE);
			break;
		}
	}
}
void Memory::PrintRegions(){
	_tprintf(_T("%s\n"), _T("Full:"));
	for (std::list<Region>::iterator i = Full.begin(); i != Full.end(); ++i)
	{
		Region current = *i;
		_tprintf(_T("Address: %x, Size: %d\n"), current.Address, current.Size);
	}
	_tprintf(_T("%s\n"), _T("Free:"));
	for (std::list<Region>::iterator i = Free.begin(); i != Free.end(); ++i)
	{
		Region current = *i;
		_tprintf(_T("Address: %x, Size: %d\n"), current.Address, current.Size);
	}
}

Pages::Pages(int size){
	this->size = size;
}
void Pages::PrintState(){
	for (std::list<int>::iterator i = pages.begin(); i != pages.end(); ++i)
	{
		DWORD current = *i;
		_tprintf(_T("%d "), current);
	}
	_tprintf(_T("\n"));
}
void Pages::UsePages(int order[], int length){
	for (DWORD i = 0; i < length; i++)
	{
		bool contains = std::find(std::begin(pages), std::end(pages), order[i]) != std::end(pages);
		// new element, enough space: add to the end
		if (!contains && (pages.size() < this->size)){
			pages.push_back(order[i]);
		}
		// old element
		else if (contains){
			pages.remove(order[i]);
			pages.push_back(order[i]);
		}
		// new element, out of space: delete the element as least recently used, add new to the end
		else if (!contains && (pages.size() == this->size)){
			pages.remove(ElementAt(0));
			pages.push_back(order[i]);
		}
		_tprintf(_T("%d\n"), order[i]);
		PrintState();
	}
};
int Pages::ElementAt(int index){
	std::list<int>::iterator it = pages.begin();
	std::advance(it, index);
	return *it;
}
void Pages::MovePageForward(int from){
	int el = ElementAt(from);
	pages.remove(el);
	pages.push_front(el);
}

NotLRU::NotLRU(int size){
	this->size = size;
	lru = new int[size];
	counters = new int[size];
	full = false;
	for (DWORD i = 0; i < size; i++)
	{
		counters[i] = 0;
		lru[i] = UNDEFINED;
	}
}
void NotLRU::Load(int order[], int length){
	for (DWORD i = 0; i < length; i++)
	{
		Load(order[i]);
		PrintState();
	}
}
void NotLRU::Load(int value){
	for (DWORD j = 0; j < size; j++)
	{
		if (!full){
			if (lru[j] != UNDEFINED){
				continue;
			}
			else{
				lru[j] = value;
				counters[j] = 0;
				Increment(j);
				if (j == size - 1){
					full = true;
				}
				break;
			}
		}
		// replacing or accessing
		else{
			bool index = IndexOf(j);
			int lruIndex = FindLRU();

			// adding new el
			if (index = -1){
				lru[lruIndex] = value;
				counters[lruIndex] = 0;
				Increment(lruIndex);
			}
			// accessing old
			else
			{
				counters[lruIndex] = 0;
				Increment(lruIndex);
			}
			break;
		}
	}
}
void NotLRU::PrintState(){
	for (int i = 0; i < size; i++){
		_tprintf(_T("%d"), counters[i]);
	}
	_tprintf(_T("\t"));
	for (int i = 0; i < size; i++){
		_tprintf(_T("%d"), lru[i]);
	}
	_tprintf(_T("\n"));
}
void NotLRU::Increment(int except){
	for (DWORD i = 0; i < size; i++)
	{
		if (i == except){
			continue;
		}
		counters[i]++;
	}
}
int NotLRU::IndexOf(int value){
	for (DWORD i = 0; i < size; i++)
	{
		if (lru[i] == value)
			return i;
	}
	return -1;
}
int NotLRU::FindLRU(){
	int maxIndex = 0;
	for (DWORD i = 0; i < size; i++)
	{
		if (counters[i] > counters[maxIndex]){
			maxIndex = i;
		}
	}
	return maxIndex;
}

LRU::LRU(){

}
void LRU::Load(int value){
	int index = GetIndex();
	values[index] = value;
}
void LRU::Load(int * order, int length){
	for (DWORD i = 0; i < length; i++)
	{
		Load(order[i]);
	}
}
int LRU::GetIndex(){
	// ����
	if (lru[0]){
		// ���� �����
		if (lru[1]){
			return 1;
		}
		// ���� ����
		else{
			return 0;
		}
	}
	// �����
	else{
		// ����� �����
		if (lru[2]){
			return 1;
		}
		// ����� ����
		else{
			return 0;
		}
	}
}
