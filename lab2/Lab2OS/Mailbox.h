#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <iostream>
#include <conio.h>

enum METHOD{CREATE, OPEN};

class Letter{
public:
	DWORD startIndex;
	DWORD length;

	Letter(DWORD startIndex, DWORD length);
};

class Mailbox{
public:
	Mailbox(TCHAR * fileName);
	Mailbox(DWORD maxMessages, TCHAR * fileName);
	~Mailbox();
	void addLetter(TCHAR content[]);
	TCHAR * readLetter(DWORD id, bool deleteThen);
	void deleteLetter(DWORD id);
	void deleteAllLetters();
	DWORD countLetters();
	DWORD generateChecksum();
	TCHAR * getInfo();
private:
	const DWORD CONTENT_START = 12;
	HANDLE handle, hMap;
	TCHAR * path = _T("C:\\\\Users\\Max\\test\\");
	TCHAR * type = _T(".txt");
	TCHAR * name;
	PVOID pMap;

	DWORD* readFileHeader();
	void writeHeader(DWORD* newHeader);
	Letter* getLetter(DWORD id);
	PBYTE Mailbox::readMemory(DWORD start, DWORD length);
};