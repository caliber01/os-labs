#include "stdafx.h"
#include <list>
#include <string>
#include <iostream>
#include "windows.h"

#define UNDEFINED 999

using namespace std;

// task1,2
class Region{
public:
	LPVOID Address;
	DWORD Size;
	Region(LPVOID address, DWORD size);
	bool operator == (Region other);
};
class Memory{
public:
	list<Region> Full;
	list<Region> Free;

	Memory();
	LPVOID AllocateSpace(DWORD size);
	void FreeSpace(LPVOID address);
	void PrintRegions();

private:
	HANDLE handle;
};

// task3
class Pages{
public:
	void UsePages(int order[], int length);
	Pages(int size);
	void PrintState();

private:
	int size;
	list<int> pages;
	int ElementAt(int index);
	void Pages::MovePageForward(int from);
};

// task4
class NotLRU{
public:
	NotLRU(int size);
	void Load(int order[], int length);
	void Load(int value);
	void PrintState();
private:
	int * lru;
	int * counters;
	int size;
	bool full;
	void Increment(int except);
	int IndexOf(int value);
	int FindLRU();
};

class LRU{
public:
	LRU();
	void Load(int value);
	void Load(int * order, int length);
	void printState();
private:
	int values[4];
	bool lru[3];
	int GetIndex();
};