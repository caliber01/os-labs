// toolhelp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "toolhelp.h"

using namespace std;

void DescribeProcesses();
void ViewProcessInfo(PROCESSENTRY32 process);
void ViewProcessModules(DWORD processId);

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "RU");
	DescribeProcesses();
	getchar();
	return 0;
}

void DescribeProcesses() {
	PROCESSENTRY32 pe;
	pe.dwSize = sizeof(PROCESSENTRY32);

	HANDLE hProcessSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	bool success = Process32First(hProcessSnapshot, &pe);
	if (!success) {
		_tprintf(_T("Error occurred\n"));
		_tprintf(_T("Last error code: %d\n"), GetLastError());
		return;
	}
	do {
		ViewProcessInfo(pe);
		ViewProcessModules(pe.th32ProcessID);
	} while (Process32Next(hProcessSnapshot, &pe));

	CloseHandle(hProcessSnapshot);
}

void ViewProcessInfo(PROCESSENTRY32 process) {
	_tprintf(_T("Executable name: %s\n"), process.szExeFile);
	_tprintf(_T("Number of threads: %d\n"), process.cntThreads);
}

void ViewProcessModules(DWORD processId) {
	HANDLE hModuleSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, processId);
	MODULEENTRY32 me;
	me.dwSize = sizeof(MODULEENTRY32);
	Module32First(hModuleSnapshot, &me);
	_tprintf(_T("Module names:\n"));
	do {
		_tprintf(_T("%s, "), me.szModule);
	} while (Module32Next(hModuleSnapshot, &me));
	_tprintf(_T("\n\n"));
	CloseHandle(hModuleSnapshot);
}

