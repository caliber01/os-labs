#pragma once

#include <vector>
#include "C://Users//������//Documents//Visual Studio 2015//Projects//os-labs//lab3//lab3//header.h"

using namespace std;


class AnalyzerFile {
public:
	AnalyzerFile(HANDLE);
	void printInfo();
private:
	HANDLE handle;
	bool isUnicode;
	vector<int> lineSizes;
	int fileSize;

	void getLines();
	void checkTextUnicode();
};


class Analyzer {
public:
	Analyzer();
	void printResults();
private:
	vector<AnalyzerFile> files;
};