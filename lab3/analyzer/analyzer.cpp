// analyzer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "analyzer.h"

int _tmain(int argc, _TCHAR* argv[])
{
	Analyzer analyzer = * new Analyzer();
	analyzer.printResults();
	getchar();

	return 0;
}

unsigned long long FileTimeToLongLong(FILETIME ft) {
	return (((unsigned long long)ft.dwHighDateTime) << 32) | ft.dwLowDateTime;
};

// analyzer
Analyzer::Analyzer() {
	WIN32_FIND_DATA fd;
	bool b;
	TCHAR * name = _T("C://Users//������//Documents//Visual Studio 2015//Projects//os-labs//lab3/*.txt");
	TCHAR * relativePath = _T("C://Users//������//Documents//Visual Studio 2015//Projects//os-labs//lab3//");

	TCHAR buffer[MAX_PATH];
	DWORD varSize = GetEnvironmentVariable(_T("MinTime"), buffer, 0);
	b = GetEnvironmentVariable(_T("MinTime"), buffer, varSize);
	
	unsigned long long minTime;
	_stscanf_s(buffer,_T("%llu\n"), &minTime);
	_tprintf(_T("got: %llu \n\n"), minTime);
	_tprintf(_T("var: %d\n"), varSize);

	HANDLE findHandle = FindFirstFile(name, &fd);
	HANDLE h = INVALID_HANDLE_VALUE;
	TCHAR * path;
	FILETIME creationTime;
	FILETIME lastAccessTime;
	FILETIME lastWriteTime;
	do
	{
		 path = new TCHAR[MAX_PATH * 2];
		_tcscpy_s(path, _tcslen(relativePath) * sizeof(TCHAR),relativePath);
		_tcscat_s(path, (_tcslen(fd.cFileName) + _tcslen(path))* sizeof(TCHAR), fd.cFileName);

		h = CreateFile(path, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

		GetFileTime(h, &creationTime, &lastAccessTime, &lastWriteTime);
		unsigned long long curFileTime = FileTimeToLongLong(creationTime);
		if (curFileTime >= minTime) {
			files.push_back(*new AnalyzerFile(h));
		}
		FindNextFile(findHandle, &fd);

	} while (GetLastError() != ERROR_NO_MORE_FILES);
}

void Analyzer::printResults() {
	for (vector<AnalyzerFile>::iterator i = files.begin(); i != files.end(); ++i) {
		(*i).printInfo();
	}
}


// file
AnalyzerFile::AnalyzerFile(HANDLE h) {
	handle = h;
	checkTextUnicode();
	getLines();
	fileSize = GetFileSize(handle, 0);
}

void AnalyzerFile::checkTextUnicode() {
	BYTE buffer[10];
	DWORD dwCount;
	SetFilePointer(handle, 2, 0, FILE_BEGIN);
	ReadFile(handle, buffer, 10, &dwCount, 0);
	isUnicode = IsTextUnicode(buffer, sizeof(buffer), 0);
}

void AnalyzerFile::getLines() {

	DWORD dwCount;
	SetFilePointer(handle, 0, 0, FILE_BEGIN);
	int lineCount = 0;

	if (isUnicode) {

		wchar_t buf[128];
		do {
			ReadFile(handle, buf, 256, &dwCount, 0);
			for (size_t i = 0; i < 128; i++, lineCount++)
			{
				if (buf[i] == L'\n') {
					lineSizes.push_back(lineCount);
					lineCount = 0;
				}
			}
		} while (dwCount == 256);
	}
	else {

		char buf[128];
		do {
			ReadFile(handle, buf, 256, &dwCount, 0);
			for (size_t i = 0; i < 256; i++, lineCount++)
			{
				if (buf[i] == '\n') {
					lineSizes.push_back(lineCount);
					lineCount = 0;
				}
			}
		} while (dwCount == 256);
	}
}

void AnalyzerFile::printInfo() {
	_tprintf(_T("file size: %d \n"), fileSize);
	_tprintf(_T("lines: %d\n"), lineSizes.size() + 1);
	_tprintf(_T("isUnicode: %d\n"), isUnicode);
	int index = 0;
	for (vector<int>::iterator i = lineSizes.begin(); i != lineSizes.end(); ++i, index++) {
		_tprintf(_T("line #%d size: %d \n"), index, *i);
	}
	_tprintf(_T("\n"));
}

