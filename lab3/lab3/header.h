#pragma once
#include <windows.h>

#define NewProcess(name, startupInfo, processInformation) CreateProcess(0, name, 0, 0, 0, 0, 0, 0, startupInfo, processInformation);
#define NewProcessSuspended(name, startupInfo, processInformation) CreateProcess(0, name, 0, 0, 0, CREATE_SUSPENDED, 0, 0, startupInfo, processInformation);


