// philosophers.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>

void eat(int index);
void think(int index);
void philosophers(int N);

int _tmain(int argc, _TCHAR* argv[])
{
	philosophers(6);
	return 0;
}

class Fork {
public:
	bool isAvailable;
};

typedef struct {
	int index;
}PAR, *PPAR;

struct {
	HANDLE * forks;
	HANDLE * threads;
	PPAR params;
	DWORD N;
} Resources;

DWORD WINAPI philosopher(LPVOID param){
	PAR par = *(PPAR)param;
	_tprintf(_T("philosopher#%d started\n"), par.index);
	
	for (int i = 0; i < 5; i++) {
		int leftForkIndex = par.index;
		int rightForkIndex = par.index+1 % Resources.N;
		int minimalFork = leftForkIndex > rightForkIndex ? rightForkIndex : leftForkIndex;
		int secondFork = leftForkIndex == minimalFork ? rightForkIndex : leftForkIndex;

		// occupy the lowindex fork
		_tprintf(_T("philosopher#%d is trying to occupy first fork #%d\n"), par.index, minimalFork);
		WaitForSingleObject(Resources.forks[minimalFork], INFINITE);
		// occupy the second fork
		_tprintf(_T("philosopher#%d is trying to occupy second fork #%d\n"), par.index, secondFork);
		WaitForSingleObject(Resources.forks[secondFork], INFINITE);
		eat(par.index);
		// release forks
		ReleaseMutex(Resources.forks[minimalFork]);
		ReleaseMutex(Resources.forks[secondFork]);
		think(par.index);
	}
	return 0;
}

void philosophers(int N) {
	Resources.N = N;
	Resources.forks = new HANDLE[N];
	Resources.threads = new HANDLE[N];
	Resources.params = new PAR[N];

	for (int i = 0; i < N; i++) {
		TCHAR name[80];
		_stprintf_s(name, _T("fork#%d"), i);
		Resources.forks[i] = CreateMutex(0, false, name);
		Resources.params[i].index = i;
		Resources.threads[i] = CreateThread(0, 0, philosopher, (PVOID)&Resources.params[i], 0, 0);
	}

	WaitForMultipleObjects(N, Resources.threads, true, INFINITE);
}

void eat(int index) {
	_tprintf(_T("philosopher#%d is eating\n"), index);
	Sleep(1000);
}

void think(int index) {
	_tprintf(_T("philosopher#%d is thinking\n"), index);
}