// writer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>

bool debug = true;

int _tmain(int argc, _TCHAR* argv[])
{
	return 0;
}

void writer() {

	HANDLE readCountMutex = OpenMutex(MUTEX_ALL_ACCESS, false, _T("readCountMutex"));
	HANDLE readersMutex = OpenMutex(MUTEX_ALL_ACCESS, false, _T("readersMutex"));
	HANDLE writersMutex = OpenMutex(MUTEX_ALL_ACCESS, false, _T("writersMutex"));
	HANDLE lastReaderSemaphore = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, _T("lastReaderSemaphore"));

	HANDLE hMapCounter = OpenFileMapping(FILE_MAP_ALL_ACCESS, false, _T("readerWriterData"));
	PVOID pMapCounter = MapViewOfFile(hMapCounter, FILE_MAP_ALL_ACCESS, 0, 0, 256);

	// TODO
	TCHAR * fileName = _T("someProcessFile.txt");
	TCHAR debugName[10];
	// TODO
	_stprintf_s(debugName, _T("WRITER%d: "), 111111);

	while (true) {
		if(debug)_tprintf(_T("%s waiting for readers in their init sections\n"), debugName);
		// wait until readers left init section and then block it
		WaitForSingleObject(readersMutex, INFINITE);
		if(debug)_tprintf(_T("%s waiting for writers mutex\n"), debugName);
		//block the resource so that many writers didn't write simultinuasly 
		WaitForSingleObject(writersMutex, INFINITE);

		if(debug)_tprintf(_T("%s block other writers, going to write\n"), debugName);
		write(fileName, 1);
		// TODO
		_tprintf(_T("writer#%d written\n"),111111);

		// let readers enter threir init section
		if(debug)_tprintf(_T("%s let readers read\n"), debugName);
		ReleaseMutex(readersMutex);
		// let other writers to write
		if(debug)_tprintf(_T("%s release other writers\n"), debugName);
		ReleaseMutex(writersMutex);
	}
}

void write(TCHAR * fileName, int globalCount) {
	HANDLE hFile = CreateFile(fileName, GENERIC_WRITE, FILE_SHARE_READ, 0, OPEN_ALWAYS, 0, 0);
	DWORD dwCount;
	TCHAR content[80];

	_stprintf_s(content, _T("Some written text %d"), globalCount++);
	DWORD expectedCount = _tcslen(content) * sizeof(TCHAR);
	WriteFile(hFile, content, expectedCount, &dwCount, 0);
	if (dwCount != expectedCount) {
		_tprintf(_T("problems with writing {%d}\n"), GetLastError());
	}
	CloseHandle(hFile);
	Sleep(2000);
}
