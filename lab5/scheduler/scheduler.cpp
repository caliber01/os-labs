// scheduler.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>

void scheduleProcesses(int N);

int _tmain(int argc, _TCHAR* argv[])
{
	scheduleProcesses(2);
	return 0;
}

void scheduleProcesses(int N) {

	TCHAR philosophers[] = _T("philosophers.exe");
	TCHAR readersWriters[] = _T("readersWriters.exe");

	HANDLE * hPhils = new HANDLE[N];
	HANDLE * hRWs = new HANDLE[N];
	
	LARGE_INTEGER li;
	li.QuadPart = -10000000;
	HANDLE hTimer = CreateWaitableTimer(0, false, _T("timer"));
	SetWaitableTimer(hTimer, &li, 10000, 0, 0, false);

	for (int i = 0; i < N; i++) {
		WaitForSingleObject(hTimer, INFINITE);
		
		STARTUPINFO siPhil;
		PROCESS_INFORMATION piPhil;

		ZeroMemory(&siPhil, sizeof(siPhil));
		siPhil.cb = sizeof(siPhil);
		ZeroMemory(&piPhil, sizeof(piPhil));

		STARTUPINFO siRW;
		PROCESS_INFORMATION piRW;

		ZeroMemory(&siRW, sizeof(siRW));
		siRW.cb = sizeof(siRW);
		ZeroMemory(&piRW, sizeof(piRW));

		_tprintf(_T("STARTING PHILOSOPHERS %d\n"), i);
		CreateProcess(0, philosophers, 0, 0, 0, 0, 0, 0, &siPhil, &piPhil);
		_tprintf(_T("STARTING READERS WRITERS %d\n"), i);
		CreateProcess(0, readersWriters, 0, 0, 0, 0, 0, 0, &siRW, &piRW);
		hPhils[i] = piPhil.hProcess;
		hRWs[i] = piRW.hProcess;
	}

	CancelWaitableTimer(hTimer);

	WaitForMultipleObjects(N, hPhils, true, INFINITE);
	WaitForMultipleObjects(N, hRWs, true, INFINITE);
	CloseHandle(hTimer);
}

