// reader.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Windows.h"

bool debug = true;

int _tmain(int argc, _TCHAR* argv[])
{
	return 0;
}

void reader() {

	HANDLE readCountMutex = OpenMutex(MUTEX_ALL_ACCESS, false, _T("readCountMutex"));
	HANDLE readersMutex = OpenMutex(MUTEX_ALL_ACCESS, false, _T("readersMutex"));
	HANDLE writersMutex = OpenMutex(MUTEX_ALL_ACCESS, false, _T("writersMutex"));
	HANDLE lastReaderSemaphore = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, _T("lastReaderSemaphore"));

	HANDLE hMap= OpenFileMapping(FILE_MAP_ALL_ACCESS, false, _T("readerWriterData"));
	PVOID pMap= MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, 256);
	PDWORD memory = (PDWORD)pMap;

	// TODO
	TCHAR * fileName = _T("someProcessFile.txt");
	TCHAR debugName[10];
	// TODO
	_stprintf_s(debugName, _T("WRITER%d: "), 11111);
	int readersCount;

	while (true) {
		if(debug)_tprintf(_T("%s waiting until writers finished\n"), debugName);
		// waiting until writers finished
		WaitForSingleObject(readersMutex, INFINITE);
		if (debug)_tprintf(_T("%s writers finished; locking counter\n"), debugName);
		// locking counter
		WaitForSingleObject(readCountMutex, INFINITE);

		// increase it
		readersCount = ++memory[0];

		// this reader is the first
		bool isFirst = readersCount == 1;
		if (isFirst) {
			if(debug)_tprintf(_T("%s this reader is the first\n"), debugName);
			if(debug)_tprintf(_T("%s lock writers from writing\n"), debugName);
			// lock writers from writing
			WaitForSingleObject(writersMutex, INFINITE);
		}
		if(debug)_tprintf(_T("%s finished working with counter\n"), debugName);
		// finished working with counter
		ReleaseMutex(readCountMutex);
		if(debug)_tprintf(_T("%s let other reader\n"), debugName);
		// this reader is done
		ReleaseMutex(readersMutex);

		TCHAR* content = read(fileName);
		_tprintf(_T("%s read: %s\n"), debugName, content);

		if(debug)_tprintf(_T("%s lock counter\n"), debugName);
		// lock counter
		WaitForSingleObject(readCountMutex, INFINITE);
		if(debug)_tprintf(_T("%s decrease counter\n"), debugName);


		// decrease it
		readersCount = --memory[0];


		// this reader is the last here
		bool isLast = readersCount == 0;
		if (isLast) {
			if (debug)_tprintf(_T("%s is last one; let the first release writers\n"), debugName);
			ReleaseSemaphore(lastReaderSemaphore, 1, 0);
		}
		// unlock counter
		if(debug)_tprintf(_T("%s unlock counter\n"), debugName);
		ReleaseMutex(readCountMutex);
		if (isFirst) {
			WaitForSingleObject(lastReaderSemaphore, INFINITE);
			if(debug)_tprintf(_T("%s releasing writers mutex\n"), debugName);
			// let writers write
			DWORD mutexRes = ReleaseMutex(writersMutex);
			if (mutexRes == 0)
			{
				if (debug)_tprintf(_T("%s ERROR can't release writers mutex (%d)\n"), debugName, GetLastError());
			}
		}
	}
}

TCHAR * read(TCHAR * fileName) {
	HANDLE hFile = CreateFile(fileName, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_ALWAYS, 0, 0);
	DWORD dwCount;
	static TCHAR content[80];
	DWORD expectedCount = _tcslen(content) * sizeof(TCHAR);
	ReadFile(hFile, content, 80, &dwCount, 0);
	CloseHandle(hFile);
	content[dwCount] = 0;
	return content;
}


