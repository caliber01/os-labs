// lab5.cpp : Defines the entry point for the console application.
//

#define _CRT_RAND_S
#include "stdafx.h"
#include <Windows.h>

#include <stdlib.h>

TCHAR * read();
void write();
void ReadersWriters(int N);

bool debug = false;

int _tmain(int argc, _TCHAR* argv[])
{
	ReadersWriters(10);
	return 0;
}

typedef struct {

	DWORD index;

} PAR, *PPAR;

struct {

	HANDLE readCountMutex;
	HANDLE writersMutex;
	HANDLE readersMutex;
	HANDLE lastReaderSemaphore;
	DWORD readersCount;
	HANDLE * writersThreads;
	HANDLE * readersThreads;
	PPAR writersParams;
	PPAR readersParams;
	TCHAR * fileName;
	DWORD globalCount;

} READERS_WRITERS;

DWORD WINAPI reader(PVOID param) {

	PPAR par = (PPAR)param;

	_tprintf(_T("reader#%d started\n"), par->index);
	TCHAR debugName[10];
	_stprintf_s(debugName, _T("READER%d: "), par->index);

	for (size_t i = 0; i < 2; i++) {
		
		if(debug)_tprintf(_T("%s waiting until writers finished\n"), debugName);
		// waiting until writers finished
		WaitForSingleObject(READERS_WRITERS.readersMutex, INFINITE);
		if (debug)_tprintf(_T("%s writers finished; locking counter\n"), debugName);
		// locking counter
		WaitForSingleObject(READERS_WRITERS.readCountMutex, INFINITE);
		READERS_WRITERS.readersCount++;
		// this reader is the first
		bool isFirst = READERS_WRITERS.readersCount == 1;
		if (isFirst) {
			if(debug)_tprintf(_T("%s this reader is the first\n"), debugName);
			if(debug)_tprintf(_T("%s lock writers from writing\n"), debugName);
			// lock writers from writing
			WaitForSingleObject(READERS_WRITERS.writersMutex, INFINITE);
		}
		if(debug)_tprintf(_T("%s finished working with counter\n"), debugName);
		// finished working with counter
		ReleaseMutex(READERS_WRITERS.readCountMutex);
		if(debug)_tprintf(_T("%s let other reader\n"), debugName);
		// this reader is done
		ReleaseMutex(READERS_WRITERS.readersMutex);

		TCHAR* content = read();
		_tprintf(_T("%s read: %s\n"), debugName, content);

		if(debug)_tprintf(_T("%s lock counter\n"), debugName);
		// lock counter
		WaitForSingleObject(READERS_WRITERS.readCountMutex, INFINITE);
		if(debug)_tprintf(_T("%s decrease counter\n"), debugName);
		// decrease it
		READERS_WRITERS.readersCount--;
		// this reader is the last here
		bool isLast = READERS_WRITERS.readersCount == 0;
		if (isLast) {
			if (debug)_tprintf(_T("%s is last one; let the first release writers\n"), debugName);
			ReleaseSemaphore(READERS_WRITERS.lastReaderSemaphore, 1, 0);
		}
		// unlock counter
		if(debug)_tprintf(_T("%s unlock counter\n"), debugName);
		ReleaseMutex(READERS_WRITERS.readCountMutex);
		if (isFirst) {
			WaitForSingleObject(READERS_WRITERS.lastReaderSemaphore, INFINITE);
			if(debug)_tprintf(_T("%s releasing writers mutex\n"), debugName);
			// let writers write
			DWORD mutexRes = ReleaseMutex(READERS_WRITERS.writersMutex);
			if (mutexRes == 0)
			{
				if (debug)_tprintf(_T("%s ERROR can't release writers mutex (%d)\n"), debugName, GetLastError());
			}
		}
	}

	return 0;

}

DWORD WINAPI writer(PVOID param) {

	PPAR par = (PPAR)param;

	TCHAR debugName[10];
	_stprintf_s(debugName, _T("WRITER%d: "), par->index);

	_tprintf(_T("writer#%d started\n"), par->index);
	for (size_t i = 0; i < 2; i++) {

		if(debug)_tprintf(_T("%s waiting for readers in their init sections\n"), debugName);
		// wait until readers left init section and then block it
		WaitForSingleObject(READERS_WRITERS.readersMutex, INFINITE);
		if(debug)_tprintf(_T("%s waiting for writers mutex\n"), debugName);
		//block the resource so that many writers didn't write simultinuasly 
		WaitForSingleObject(READERS_WRITERS.writersMutex, INFINITE);

		if(debug)_tprintf(_T("%s block other writers, going to write\n"), debugName);
		write();
		_tprintf(_T("writer#%d written\n"), par->index);

		// let readers enter threir init section
		if(debug)_tprintf(_T("%s let readers read\n"), debugName);
		ReleaseMutex(READERS_WRITERS.readersMutex);
		// let other writers to write
		if(debug)_tprintf(_T("%s release other writers\n"), debugName);
		ReleaseMutex(READERS_WRITERS.writersMutex);
	}
	
	return 0;
}

void write() {
	HANDLE hFile = CreateFile(READERS_WRITERS.fileName, GENERIC_WRITE, FILE_SHARE_READ, 0, OPEN_ALWAYS, 0, 0);
	DWORD dwCount;
	TCHAR content[80];
	unsigned int random;
	rand_s(&random);
	_stprintf_s(content, _T("Some written news %d %d\r\n"), READERS_WRITERS.globalCount++, 10000000 / (random % 1000));
	DWORD expectedCount = _tcslen(content) * sizeof(TCHAR);
	SetFilePointer(hFile, 0, 0, FILE_END);
	WriteFile(hFile, content, expectedCount, &dwCount, 0);
	if (dwCount != expectedCount) {
		_tprintf(_T("problems with writing {%d}\n"), GetLastError());
	}
	CloseHandle(hFile);
	Sleep(2000);
}

TCHAR * read() {
	HANDLE hFile = CreateFile(READERS_WRITERS.fileName, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_ALWAYS, 0, 0);
	DWORD dwCount;
	static TCHAR content[160];
	SetFilePointer(hFile, -160, 0, FILE_END);
	ReadFile(hFile, content, 160*sizeof(TCHAR), &dwCount, 0);
	CloseHandle(hFile);
	dwCount = dwCount / sizeof(TCHAR);
	content[dwCount] = 0;
	if (dwCount != 0) {
		int newsStart = 0;
		for (int i = dwCount - 3; i >= 0; i--)
		{
			if (content[i] == _T('\n')) {
				newsStart = i + 1;
				break;
			}
		}
		static TCHAR * news = new TCHAR[dwCount - newsStart];
		for (int i = newsStart, j = 0; i < dwCount; i++, j++)
		{
			news[j] = content[i];
		}
		news[dwCount - newsStart] = 0;
		return news;
	}
	return content;
}

void ReadersWriters(int N) {

	READERS_WRITERS.readersCount = 0;
	READERS_WRITERS.readCountMutex = CreateMutex(0, false, _T("readCountMutex"));
	READERS_WRITERS.readersMutex = CreateMutex(0, false, _T("readersMutex"));
	READERS_WRITERS.writersMutex = CreateMutex(0, false, _T("writersMutex"));
	READERS_WRITERS.lastReaderSemaphore = CreateSemaphore(0, 0, 1, _T("lastReaderSemaphore"));

	READERS_WRITERS.readersThreads = new HANDLE[N];
	READERS_WRITERS.writersThreads = new HANDLE[N];
	READERS_WRITERS.readersParams = new PAR[N];
	READERS_WRITERS.writersParams = new PAR[N];

	READERS_WRITERS.globalCount = 0;

	READERS_WRITERS.fileName = _T("someFile.txt");
	HANDLE hFile = CreateFile(READERS_WRITERS.fileName, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, 0, 0);
	CloseHandle(hFile);

	for (int i = 0; i < N; i++) {
		READERS_WRITERS.readersParams[i].index = i;
		READERS_WRITERS.readersThreads[i] = CreateThread(0, 0, reader, (PVOID)&READERS_WRITERS.readersParams[i], 0, 0);

		READERS_WRITERS.writersParams[i].index = i;
		READERS_WRITERS.writersThreads[i] = CreateThread(0, 0, writer, (PVOID)&READERS_WRITERS.writersParams[i], 0, 0);
	}

	WaitForMultipleObjects(N, READERS_WRITERS.readersThreads, true, INFINITE);
	WaitForMultipleObjects(N, READERS_WRITERS.writersThreads, true, INFINITE);
}

void ReadersWritersProcesses(int N) {

	int readersCount = 0;
	HANDLE readCountMutex = CreateMutex(0, false, _T("readCountMutex"));
	HANDLE readersMutex = CreateMutex(0, false, _T("readersMutex"));
	HANDLE writersMutex = CreateMutex(0, false, _T("writersMutex"));
	HANDLE lastReaderSemaphore = CreateSemaphore(0, 0, 1, _T("lastReaderSemaphore"));

	HANDLE * readersProcesses = new HANDLE[N];
	HANDLE * writersProcesses = new HANDLE[N];

	TCHAR * fileName = _T("someProcessFile.txt");
	HANDLE hFile = CreateFile(fileName, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, 0, 0);
	CloseHandle(hFile);

	//HANDLE hMapFilename = CreateFileMapping(INVALID_HANDLE_VALUE, 0, PAGE_READWRITE, 0, 256, _T("fileName"));
	//PVOID pMapFilename = MapViewOfFile(hMapFilename, FILE_MAP_ALL_ACCESS, 0, 0, 256);
	//PDWORD mem = (PDWORD)pMapFilename;
	//mem[0] = sizeof(fileName);
	//TCHAR * memText = (TCHAR *)pMapFilename;
	//_tcscpy_s(memText, _tcslen(fileName), fileName);
	//UnmapViewOfFile(pMapFilename);

	HANDLE hMapCounter = CreateFileMapping(INVALID_HANDLE_VALUE, 0, PAGE_READWRITE, 0, 256, _T("readerWriterData"));
	PVOID pMapCounter = MapViewOfFile(hMapCounter, FILE_MAP_ALL_ACCESS, 0, 0, 256);
	PDWORD mem = (PDWORD)pMapCounter;
	mem[0] = 0; // readerCounter
	mem[1] = 1; // globalCounter
	UnmapViewOfFile(pMapCounter);

	TCHAR readerCmd[] = _T("reader.exe");
	TCHAR writerCmd[] = _T("writer.exe");

	for (int i = 0; i < N; i++) {

		STARTUPINFO readSi;
		PROCESS_INFORMATION readPi;

		ZeroMemory( &readSi, sizeof(readSi) );
		readSi.cb = sizeof(readSi);
		ZeroMemory( &readPi, sizeof(readPi) );	

		STARTUPINFO writeSi;
		PROCESS_INFORMATION writePi;

		ZeroMemory( &writeSi, sizeof(writeSi) );
		writeSi.cb = sizeof(writeSi);
		ZeroMemory( &writePi, sizeof(writePi) );	

		CreateProcess(0, readerCmd, 0, 0, 0, 0, 0, 0, &readSi, &readPi);
		CreateProcess(0, writerCmd, 0, 0, 0, 0, 0, 0, &writeSi, &writePi);
		readersProcesses[i] = readPi.hProcess;
		writersProcesses[i] = writePi.hProcess;
	}

	WaitForMultipleObjects(N, readersProcesses, true, INFINITE);
	WaitForMultipleObjects(N, writersProcesses, true, INFINITE);
}